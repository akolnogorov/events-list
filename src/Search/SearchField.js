import React from 'react';
import './SearchField.css';

const SearchField = (props) => {
	const { searchValue, searchEvent, resetSearchEvent } = props;

	return (
		<div className="SearchField">
			<input
				className="SearchField_input" type="text" placeholder="Найти событие..."
				value={searchValue} onChange={searchEvent}
			/>
			<button className="SearchField_reset-btn" onClick={resetSearchEvent}>Сбросить</button>
		</div>
	);
};

export default SearchField;
