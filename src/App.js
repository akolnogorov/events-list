import React from 'react';
import './App.css';
import data from './data';
import Menu from './Menu/Menu';
import EventsList from './EventsList/EventsList';
import EventDetails from './EventsList/Event/EventDetails';
import Overlay from './EventsList/Event/Overlay.js';
import SearchField from './Search/SearchField';

class App extends React.Component {
	constructor(props) {
		super(props);

		const uniqueTypes = ['Все', ...new Set(data.map(event => event.type))];

		this.types = uniqueTypes.reduce((obj, type, i) => {
			obj[type] = i;
			return obj;
		}, {});

		this.openedEventData = {};

		this.state = {
			showMode: 'all',
			sortMode: 'default',
			typeMode: 0,
			favourites: Array(data.length).fill(false),
			eventDetailsIsOpened: false,
			searchValue: ''
		};
	}

	changeFavouriteState = (number, e) => {
		if (e) {
			e.stopPropagation();
		}

		const favourites = [...this.state.favourites];

		favourites[number] = !favourites[number];

		this.setState({
			favourites: favourites
		});
	}

	showModeChange = (showMode) => {
		this.setState({ showMode: showMode });
	}

	clearFavourites = () => {
		this.setState({ favourites: Array(data.length).fill(false) });
	}

	sortModeChange = (sortMode) => {
		this.setState({ sortMode: sortMode });
	}

	typeModeChange = (typeMode) => {
		this.setState({ typeMode: typeMode });
	}

	openEventDetails = (eventData) => {
		this.setState({ eventDetailsIsOpened: true });
		this.openedEventData = eventData;
	}

	closeEventDetails = () => {
		this.setState({ eventDetailsIsOpened: false });
	}

	searchEvent = (e) => {
		this.setState({ searchValue: e.target.value });
	}

	resetSearchEvent = () => {
		this.setState({ searchValue: '' });
	}

	render() {
		const { showMode, sortMode, typeMode, searchValue } = this.state;

		const events = data.map((event, i) => {
			event.isFavourite = this.state.favourites[i];
			event.number = i;
			return event;
		});

		return (
			<div className="Events">
				<Menu
					types={this.types}
					showMode={showMode}
					showModeChange={this.showModeChange}
					sortMode={sortMode}
					sortModeChange={this.sortModeChange}
					typeMode={typeMode}
					typeModeChange={this.typeModeChange}
					clearFavourites={this.clearFavourites}
				/>

				<SearchField
					searchValue={searchValue}
					searchEvent={this.searchEvent}
					resetSearchEvent={this.resetSearchEvent}
				/>

				<EventsList
					events={events}
					types={this.types}
					showMode={showMode}
					sortMode={sortMode}
					typeMode={typeMode}
					searchValue={searchValue}
					changeFavouriteState={this.changeFavouriteState}
					openEventDetails={this.openEventDetails}
				/>

				{this.state.eventDetailsIsOpened &&
					<React.Fragment>
						<EventDetails
							eventData={this.openedEventData}
							closeEventDetails={this.closeEventDetails}
							changeFavouriteState={this.changeFavouriteState}
						/>
						<Overlay closeEventDetails={this.closeEventDetails} />
					</React.Fragment>
				}
			</div>
		);
	}
}

export default App;
