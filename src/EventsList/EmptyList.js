import React from 'react';
import './EmptyList.css';

const EmptyList = (props) => {
	const { mode } = props;
	const message = (mode === 'favourites') ? 'Нет избранных событий' : 'Событий не найдено';

	return (
		<div className="EmptyList">
			<h1>{message}</h1>
		</div>
	);
};

export default EmptyList;
