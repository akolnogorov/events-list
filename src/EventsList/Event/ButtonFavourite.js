import React from 'react';

const ButtonFavourite = (props) => {
	const { isFavourite, changeFavouriteState } = props;

	const favouriteBtnClass = 'Event_favourite-btn Event_favourite-btn--' + (isFavourite ? 'active' : 'inactive');

	return <span className={favouriteBtnClass} onClick={changeFavouriteState}></span>;
};

export default ButtonFavourite;
