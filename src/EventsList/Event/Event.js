import React from 'react';
import './Event.css';
import ButtonFavourite from './ButtonFavourite';

const Event = (props) => {
	const { eventData, changeFavouriteState, openEventDetails } = props;
	const { title, description, price, type, isFavourite, number } = eventData;

	const currency = '\u20bd';

	return (
		<div className="Event" onClick={() => openEventDetails(eventData)}>
			<h2 className="Event_title">{title}</h2>
			<p className="Event_description">{description}</p>
			<div className="Event_price">
				{price} {currency}
			</div>
			<div className="Event_type">
				{type}
			</div>
			<ButtonFavourite
				isFavourite={isFavourite}
				changeFavouriteState={(e) => changeFavouriteState(number, e)}
			/>
		</div>
	);
};

export default Event;
