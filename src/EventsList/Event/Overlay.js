import React from 'react';
import './Overlay.css';

const Overlay = (props) => {
	const { closeEventDetails } = props;

	return (
		<div className="Overlay" onClick={closeEventDetails}></div>
	);
};

export default Overlay;
