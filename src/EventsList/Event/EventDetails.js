import React from 'react';
import './EventDetails.css';
import ButtonFavourite from './ButtonFavourite';

const EventDetails = (props) => {
	const { eventData, changeFavouriteState } = props;
	const { title, description, price, type, text, isFavourite, number } = eventData;

	const currency = '\u20bd';

	return (
		<div className="EventDetails">
			<h1>{title}</h1>
			<div className="EventDetails_info">
				<div className="EventDetails_price">
					{price} {currency}
				</div>
				<div className="EventDetails_type">
					{type}
				</div>
			</div>
			<p className="EventDetails_description">
				{description}
				</p>
			<p className="EventDetails_text">
				{text}
			</p>
			<ButtonFavourite
				isFavourite={isFavourite}
				changeFavouriteState={() => changeFavouriteState(number)}
			/>
		</div>
	);
};

export default EventDetails;
