import React from 'react';
import './EventsList.css';
import Event from './Event/Event';
import EmptyList from './EmptyList.js';

const EventsList = (props) => {
	const { events, types, showMode, sortMode, typeMode, searchValue } = props;
	const { changeFavouriteState, openEventDetails } = props;

	const eventsSearchFiltered = events.filter(event =>
		!searchValue || ~event.title.toLowerCase().indexOf(searchValue.toLowerCase())
	);

	const eventsShowFiltered = eventsSearchFiltered.filter(event =>
		showMode === 'all' || (showMode === 'favourites' && event.isFavourite) ? event : ''
	);

	const eventsTypeFiltered = eventsShowFiltered.filter(event =>
		typeMode === 0 || (typeMode === types[event.type]) ? event : ''
	);

	let eventsSorted = [...eventsTypeFiltered];

	if (sortMode === 'increase') {
		eventsSorted = [...eventsTypeFiltered].sort((a, b) => a.price - b.price);
	} else if (sortMode === 'decrease') {
		eventsSorted = [...eventsTypeFiltered].sort((a, b) => b.price - a.price);
	}

	const eventsList = eventsSorted.map((event) =>
		<Event
			key={event.title}
			eventData={event}
			changeFavouriteState={changeFavouriteState}
			openEventDetails={openEventDetails}
		/>
	);

	const emptyList = <EmptyList mode={showMode} />;

	return (
		<div className="EventsList">
			{eventsList.length ? eventsList : emptyList}
		</div>
	);
};

export default EventsList;
