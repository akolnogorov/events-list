import React from 'react';

const Button = (props) => {
	const { className, clickHandler, text } = props;

	return (
		<button className={className} onClick={clickHandler}>
			{text}
		</button>
	);
};

export default Button;
