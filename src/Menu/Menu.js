import React from 'react';
import './Menu.css';
import Button from './Button/Button';

const Menu = (props) => {
	const { showMode, sortMode, typeMode } = props;
	const { showModeChange, sortModeChange, typeModeChange, clearFavourites } = props;
	const { types } = props;

	const allClassName = `Menu_btn ${(showMode === 'all') ? 'active' : ''}`;
	const favouritesClassName = `Menu_btn ${(showMode === 'favourites') ? 'active' : ''}`;

	const defaultClassName = `Menu_btn ${(sortMode === 'default') ? 'active' : ''}`;
	const decreaseClassName = `Menu_btn ${(sortMode === 'decrease') ? 'active' : ''}`;
	const increaseClassName = `Menu_btn ${(sortMode === 'increase') ? 'active' : ''}`;

	const typeClassNames = Object.values(types).map((type) => {
		return `Menu_btn ${(typeMode === type) ? 'active' : ''}`;
	});

	return (
		<div className="Menu">
			<div className="Menu_row">
				<Button
					className={allClassName}
					clickHandler={() => showModeChange('all')}
					text="Все события"
				/>
				<Button
					className={favouritesClassName}
					clickHandler={() => showModeChange('favourites')}
					text="Избранные"
				/>
				<Button
					className="Menu_btn"
					clickHandler={() => clearFavourites()}
					text="Очистить избранные"
				/>
			</div>
			<div className="Menu_row">
				<Button
					className={defaultClassName}
					clickHandler={() => sortModeChange('default')}
					text="По умолчанию"
				/>
				<Button
					className={decreaseClassName}
					clickHandler={() => sortModeChange('decrease')}
					text="По убыванию цены"
				/>
				<Button
					className={increaseClassName}
					clickHandler={() => sortModeChange('increase')}
					text="По возрастанию цены"
				/>
			</div>
			<div className="Menu_row">
				{Object.keys(types).map((type, i) =>
					<Button
						key={type}
						className={typeClassNames[i]}
						clickHandler={() => typeModeChange(i)}
						text={type}
					/>
				)}
			</div>
		</div>
	);
};

export default Menu;
